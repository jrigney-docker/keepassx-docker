
Dockerized [KeePassX](https://www.keepassx.org/).

## Usage

```
docker run --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY \ -v $(pwd):/files jrigney/keepassx secret.kdb
```
