FROM debian:stretch

RUN apt-get update && apt-get install -y keepassx &&\
    rm -rf /var/lib/apt/lists/* && \
    groupadd -g 1000 someone &&\
    useradd -g 1000 -u 1000 someone


ENV QT_X11_NO_MITSHM=1

USER someone

VOLUME /files
WORKDIR /files
ENTRYPOINT ["keepassx"]
